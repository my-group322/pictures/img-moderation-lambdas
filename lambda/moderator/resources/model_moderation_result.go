package resources

type ModerationResult struct {
	Picture Picture           `json:"picture"`
	Outcome ModerationOutcome `json:"outcome"`
}

type Picture struct {
	Key    string `json:"key"`
	Bucket string `json:"bucket"`
}

type ModerationOutcome int64

const (
	ModerationOutcomeFail ModerationOutcome = iota
)
