module gitlab.com/my-group322/pictures/img-moderation-lambdas/lambda/moderator

go 1.16

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/aws/aws-sdk-go-v2 v1.11.2
	github.com/aws/aws-sdk-go-v2/config v1.6.1
	github.com/aws/aws-sdk-go-v2/credentials v1.6.4
	github.com/aws/aws-sdk-go-v2/feature/cloudfront/sign v1.3.2
	github.com/aws/aws-sdk-go-v2/service/sqs v1.7.2
	github.com/aws/aws-sdk-go-v2/service/ssm v1.17.1
	github.com/sethvargo/go-envconfig v0.3.5
	github.com/sirupsen/logrus v1.8.1
)
