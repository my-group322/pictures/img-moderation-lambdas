package main

type ResponseWrapper struct {
	Rsp Response `json:"rsp"`
}

type Response struct {
	Weapon    float64 `json:"weapon,string"`
	Alcohol   float64 `json:"alcohol,string"`
	Drugs     float64 `json:"drugs,string"`
	Nudity    float64 `json:"nudity,string"`
	Offensive float64 `json:"offensive,string"`
}
