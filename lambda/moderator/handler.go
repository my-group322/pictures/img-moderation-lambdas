package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/my-group322/pictures/img-moderation-lambdas/lambda/moderator/resources"

	"github.com/aws/aws-sdk-go-v2/feature/cloudfront/sign"

	"github.com/aws/aws-sdk-go-v2/aws"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/sirupsen/logrus"
)

type Handler struct {
	log        *logrus.Logger
	config     Config
	httpClient *http.Client
	sqsClient  *sqs.Client
	cfSigner   *sign.URLSigner
}

type Config struct {
	ApiTarget      string `env:"API_TARGET"`
	PicsPrefixName string `env:"CF_NAME"`
	SqsURL         string `env:"SQS_URL"`
	Secrets
}

type Secrets struct {
	ApiKey string `json:"API_KEY"`
}

func NewHandler(log *logrus.Logger, config Config, client *http.Client, sqsClient *sqs.Client, cfSigner *sign.URLSigner) Handler {
	return Handler{
		log:        log,
		config:     config,
		httpClient: client,
		sqsClient:  sqsClient,
		cfSigner:   cfSigner,
	}
}

func (h Handler) Handle(event events.S3Event) error {
	picKey := event.Records[0].S3.Object.Key
	picLink := fmt.Sprintf("%s%s/%s", "https://", h.config.PicsPrefixName, picKey)

	log := h.log.WithField("pic_key", picKey)

	signedLink, err := h.cfSigner.Sign(picLink, time.Now().Add(5*time.Minute))
	if err != nil {
		log.WithError(err).Error("failed to sign link")
		return err
	}

	req, err := http.NewRequest(http.MethodPost, h.config.ApiTarget, nil)
	if err != nil {
		log.WithError(err).Error("failed to create request")
		return err
	}

	populateQueryParams(req, map[string]string{
		"api_key": h.config.ApiKey,
		"format":  "json",
		"cats":    "nudity,wad,offensive",
		"imgurl":  signedLink,
		"method":  "webpurify.aim.imgcheck",
	})

	resp, err := h.httpClient.Do(req)
	if err != nil {
		log.WithError(err).Error("failed to do request")
		return err
	}

	result := &ResponseWrapper{}
	if err = json.NewDecoder(resp.Body).Decode(result); err != nil {
		log.WithError(err).Error("failed to decode response")
		return err
	}

	log.Info("result weapon:", result.Rsp.Weapon)

	if notSafe(result.Rsp) {
		messageBody, err := json.Marshal(resources.ModerationResult{
			Picture: resources.Picture{
				Bucket: event.Records[0].S3.Bucket.Name,
				Key:    picKey,
			},
			Outcome: resources.ModerationOutcomeFail,
		})
		if err != nil {
			log.WithError(err).Error("failed to marshal message")
			return err
		}

		_, err = h.sqsClient.SendMessage(context.TODO(), &sqs.SendMessageInput{
			MessageBody: aws.String(string(messageBody)),
			QueueUrl:    aws.String(h.config.SqsURL),
		})
		if err != nil {
			log.WithError(err).Error("failed to send message to sqs")
			return err
		}
	}

	return nil
}

func notSafe(result Response) bool {
	return result.Nudity > 35 || result.Weapon > 35 || result.Drugs > 35 || result.Alcohol > 35 || result.Offensive > 35
}

func populateQueryParams(r *http.Request, params map[string]string) {
	q := r.URL.Query()

	for key, value := range params {
		q.Add(key, value)
	}

	r.URL.RawQuery = q.Encode()
}
