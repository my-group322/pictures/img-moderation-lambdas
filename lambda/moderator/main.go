package main

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"os"

	"github.com/aws/aws-sdk-go-v2/credentials"

	"github.com/aws/aws-sdk-go-v2/service/ssm"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/cloudfront/sign"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/sethvargo/go-envconfig"
	"github.com/sirupsen/logrus"
)

func main() {
	ctx := context.Background()
	log := logrus.New()

	cfg, err := config.LoadDefaultConfig(ctx, config.WithCredentialsProvider(
		credentials.NewStaticCredentialsProvider("AKIA3KISB6M7ALUHBA66", "fZop4pOTcisDTMvYaS+w1yrERo2iWaDwoBDwZMe1", "")))
	if err != nil {
		log.WithError(err).Error("failed to load config")
		return
	}

	secretManager := ssm.NewFromConfig(cfg)

	secretsRaw, err := secretManager.GetParameter(ctx, &ssm.GetParameterInput{
		Name:           aws.String(os.Getenv("AWS_SECRETS_ID")),
		WithDecryption: true,
	})
	if err != nil {
		log.WithError(err).Error("failed to retrieve secrets")
		return
	}

	var secrets Secrets
	err = json.Unmarshal([]byte(*secretsRaw.Parameter.Value), &secrets)
	if err != nil {
		log.WithError(err).Error("failed to unmarshal secrets")
		return
	}

	conf := Config{Secrets: secrets}
	if err = envconfig.Process(ctx, &conf); err != nil {
		log.WithError(err).Error("failed to load config")
		return
	}

	cfSecret, err := secretManager.GetParameter(ctx, &ssm.GetParameterInput{
		Name:           aws.String(os.Getenv("CF_SECRETS_ID")),
		WithDecryption: true,
	})
	if err != nil {
		log.WithError(err).Error("failed to retrieve cf secrets")
		return
	}

	cfSecretKey, err := sign.LoadPEMPrivKey(bytes.NewBuffer([]byte(*cfSecret.Parameter.Value)))
	if err != nil {
		log.WithError(err).Error("failed to load cf key")
		return
	}

	handler := NewHandler(
		log, conf, &http.Client{}, sqs.NewFromConfig(cfg),
		sign.NewURLSigner(os.Getenv("CF_PUBLIC_KEY_ID"), cfSecretKey),
	).Handle

	lambda.Start(handler)
}
