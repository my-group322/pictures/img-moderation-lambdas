import * as cdk from "@aws-cdk/core";
import lambda = require('@aws-cdk/aws-lambda');
import { RetentionDays } from "@aws-cdk/aws-logs";
import * as s3 from '@aws-cdk/aws-s3'
import {FunctionProps} from "@aws-cdk/aws-lambda/lib/function";

export function newLambda(id: string, scope: cdk.Stack, opts?: lambda.FunctionOptions): lambda.Function {
    const funcOpts = <FunctionProps>{
        code: lambda.Code.fromAsset("./artifacts/moderator.zip"),
        handler: 'handler',
        runtime: lambda.Runtime.GO_1_X,
        logRetention: RetentionDays.ONE_WEEK,
        tracing: lambda.Tracing.ACTIVE,
        memorySize: 128,
        timeout: cdk.Duration.seconds(30),
        deadLetterQueueEnabled: true
    }
    Object.assign(funcOpts, opts)

    return new lambda.Function(scope, id, funcOpts);
}

export function createBucket(stack: cdk.Stack, id: string, name: string): s3.Bucket {
    return new s3.Bucket(stack, id, {
        bucketName: name,
        publicReadAccess: false,
        blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
        removalPolicy: cdk.RemovalPolicy.DESTROY,
        encryption: s3.BucketEncryption.S3_MANAGED,
    })
}
