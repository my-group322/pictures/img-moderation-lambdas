import * as cdk from '@aws-cdk/core'
import { Role, ServicePrincipal, ManagedPolicy, RoleProps } from '@aws-cdk/aws-iam'
import { createBucket, newLambda } from './helpers'
import { S3EventSource } from '@aws-cdk/aws-lambda-event-sources'
import * as s3 from '@aws-cdk/aws-s3'
import * as cloudfront from '@aws-cdk/aws-cloudfront'
import * as sqs from '@aws-cdk/aws-sqs'
import * as origins from '@aws-cdk/aws-cloudfront-origins'
import { QueueProps } from '@aws-cdk/aws-sqs/lib/queue'
import { Duration } from '@aws-cdk/core'
import { BehaviorOptions, DistributionProps, IKeyGroup } from '@aws-cdk/aws-cloudfront'

export class CdkStack extends cdk.Stack {
  props: any

  constructor(scope: cdk.Construct, id: string, props: any, stackProps?: cdk.StackProps) {
    super(scope, id, stackProps)
    this.props = props

    const lambdaId = `${id}-lambda`
    const picsBucketName = 'fa-pictures-bucket'
    const picturesBucket = createBucket(this, `${lambdaId}-${picsBucketName}`, picsBucketName)

    const cfDistId = `${picsBucketName}-dist`
    const cfDist = new cloudfront.Distribution(this, cfDistId, <DistributionProps>{
      defaultBehavior: <BehaviorOptions>{
        origin: new origins.S3Origin(picturesBucket),
        trustedKeyGroups: [
          <IKeyGroup>{
            keyGroupId: this.props['cfKeyGroupId'],
          },
        ],
      },
    })

    const sqsDLQ = new sqs.Queue(this, `${id}-sqs-dlq`, <QueueProps>{
      queueName: `${id}-results-dlq`,
      retentionPeriod: Duration.days(2),
    })

    const sqsQ = new sqs.Queue(this, `${id}-sqs`, <QueueProps>{
      queueName: `${id}-results`,
      retentionPeriod: Duration.days(1),
      receiveMessageWaitTime: Duration.seconds(5),
      deadLetterQueue: {
        queue: sqsDLQ,
        maxReceiveCount: 2,
      },
    })

    const lambdaRole = this.newLambdaRole(lambdaId)
    picturesBucket.grantRead(lambdaRole)
    sqsQ.grantSendMessages(lambdaRole)

    const lambda = newLambda(lambdaId, this, {
      environment: {
        API_TARGET: this.props['apiTarget'],
        AWS_SECRETS_ID: `${lambdaId}-secrets`,
        CF_SECRETS_ID: this.props['cfSecretsName'],
        CF_NAME: cfDist.distributionDomainName,
        CF_PUBLIC_KEY_ID: this.props['cfPublicKeyId'],
        SQS_URL: sqsQ.queueUrl,
      },
      role: lambdaRole,
      timeout: cdk.Duration.seconds(30),
    })

    lambda.addEventSource(
      new S3EventSource(picturesBucket, {
        events: [s3.EventType.OBJECT_CREATED],
      }),
    )
  }

  newLambdaRole(lambdaId: string): Role {
    return new Role(this, `${lambdaId}-role`, <RoleProps>{
      roleName: `${lambdaId}-role`,
      assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
      managedPolicies: [ManagedPolicy.fromAwsManagedPolicyName('AWSLambdaExecute')],
    })
  }
}
