#!/bin/sh

mkdir cdk/artifacts

cd lambda || return

for dir in *; do
    (
        cd "$dir" || return

        CGO_ENABLED=0 GOOS=linux go build -o handler ./
        zip ../../cdk/artifacts/"$dir.zip" handler
    )
done